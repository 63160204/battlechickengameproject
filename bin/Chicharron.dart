import 'BerSerk.dart';
import 'Fighter.dart';

class Chicharron implements BerSerk, Fighter {
  @override
  int atk = 15;

  @override
  int hp = 200;

  @override
  int skill = 0;

  @override
  int attack() {
    var atk = 15 ;
    return atk;
  }

  @override
  int berSerk() {
    print("Chicharron Use Skill BERSERK!!");
    var BS = 30;
    return BS;

  }

  @override
  int rage() {
    var R = 10;
    return R;
  }

  @override
  void die() {
    print("\n"+
              "⢀⢀⣶⣶⣶⣶⣆⢀⢀⢀⢀⢀⣴⣦⡀⢀⢀⢀⣠⣶⣄⢀⢀⢀⣠⣶⣄⢀⢀⣶⣶⣶⣶⣶⣄"+"\n"+
              "⣾⣿⠁⢀⢀⠉⠉⠉⢀⢀⣴⣿⡿⢿⣿⣦⡀⢀⣿⣿⡿⣷⣶⣾⢿⣿⣿⢀⢸⣿⡇⢀⢀⠉⠉"+"\n"+
              "⣿⣿⢀⢀⠠⣤⣤⣤⢀⢸⣿⣇⣀⣀⣸⣿⡇⢀⣿⣿⡇⠈⠿⠁⢸⣿⣿⢀⢸⣿⡿⠿⠇"+"\n"+
              "⠻⣿⣄⢀⢀⢹⣿⡿⢀⢸⣿⡟⠛⠛⢻⣿⡇⢀⣿⣿⡇⢀⢀⢀⢸⣿⣿⢀⢸⣿⡇"+"\n"+
              "⢀⠈⠛⠛⠛⠛⠋⢀⢀⠈⠛⠃⢀⢀⠘⠛⠁⢀⠉⠛⠃⢀⢀⢀⠘⠛⠉⢀⠈⠛⠛⠛⠛⠛⠋"+"\n"+
              "\n"+
              "⢀⢀⣠⣾⠿⠿⢿⣦⡀⢀⢀⣾⣿⢀⢀⣾⣷⡆⢀⣾⣿⠿⠿⢿⣷⡆⢀⣾⣿⠿⠿⢿⣦⡀"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡆⢀⣿⣿⢀⢀⣿⣿⡇⢀⣿⣿⣀⣀⢀⢀⢀⢀⣿⣿⢀⢀⣠⣿⠇"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡇⢀⢿⣿⢀⢀⣿⣿⠇⢀⣿⣿⠛⠛⢀⢀⢀⢀⣿⣿⣷⣾⡟⠁"+"\n"+
              "⢀⠈⠻⣷⣤⣤⣴⣿⠋⠁⢀⠘⢿⣦⣴⣿⠃⢀⢀⣿⣿⣤⣤⣴⣶⡆⢀⣿⣿⠉⢻⣿⣦⡀"+"\n"+
              "⢀⢀⢀⠈⠉⠉⠉⢀⢀⢀⢀⢀⢀⠉⠉⢀⢀⢀⢀⠈⠉⠉⠉⠉⠁⢀⢀⠈⠉⢀⢀⠈⠉⠁"+
              "\n"
);
  }

}
