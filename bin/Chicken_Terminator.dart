import 'OverFlow.dart';
import 'Fighter.dart';

class Chicken_Terminator implements OverFlow, Fighter {
  @override
  int atk = 15;

  @override
  int hp = 200;

  @override
  int skill = 0;

  @override
  int attack() {
    var atk = 15;
    return atk;
  }

  @override
  void die() {
    print("\n"+
            "⢠⣦⢀⢀⢀⢠⣶⣶⡄⢀⢀⣶⣶⣶⣦⢀⢀⢰⡆⢀⢀⢰⣶⣦⢀⢀⢀⢠⣶⢀⢀⢀⢀⣶⣶⡄⢀⣶⣶⡄⢠⣦⢀⢀⢀⢀⣶⣶⡄⢀⣶⣶⡄"+"\n"+
            "⢸⣿⢀⢀⢀⣸⣿⣿⡇⢸⣿⢀⢀⢸⣿⣿⢀⢸⡇⢀⢀⢸⣿⣿⢀⢀⢀⢸⣿⢀⢀⢀⢀⣿⣿⡇⢀⣿⣿⡇⢸⣿⠿⣀⡀⢀⣿⣿⡇⢀⣿⣿⡇"+"\n"+
            "⠈⠉⣶⣶⣾⣿⣿⠉⠁⢸⣿⢀⢀⢸⣿⣿⢀⢸⡇⢀⢀⢸⣿⣿⢀⢀⢀⢸⣿⢀⢰⡆⢀⣿⣿⠁⢀⣿⡿⡇⢸⣿⢀⠈⢱⣶⣿⣿⡇⢀⣿⣿⡇"+"\n"+
            "⢀⢀⢀⣿⣿⡇⢀⢀⢀⢸⣿⢀⢀⢸⣿⣿⢀⢸⡇⢀⢀⢸⣿⣿⢀⢀⢀⢸⣿⢀⢸⡇⢀⣿⣿⡇⢀⣿⣿⡇⢸⣿⢀⢀⢀⢀⣿⣿⡇⢀⠛⠛⠃"+"\n"+
            "⢀⢀⢀⠿⢿⡇⢀⢀⢀⠈⠉⠶⣶⣾⡿⠉⢀⠈⠱⠶⣶⣿⠏⠉⢀⢀⢀⢀⠉⠶⠎⠱⠶⣿⠉⠁⢀⠿⣿⠇⠸⠿⢀⢀⢀⢀⠿⣿⠇⢀⠶⣶⠄"+"\n"); 
  }
  @override
  int overFlow() {
    print("Chicken Terminator Use Skill OVERFLOW!!");
    var BS = 20;
    return BS;
  }

  @override
  int rage() {
    var R1 = 5;
    return R1;
  }
}
