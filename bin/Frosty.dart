import 'FrostCape.dart';
import 'Fighter.dart';

class Frosty implements FrostCape, Fighter{
  @override
  int atk = 10;

  @override
  int hp = 220;

  @override
  int skill = 0;

  @override
  int attack() {
    var atk = 10 ;
    return atk;
  }

  @override
  void die() {
    print("\n"+
              "⢀⢀⣶⣶⣶⣶⣆⢀⢀⢀⢀⢀⣴⣦⡀⢀⢀⢀⣠⣶⣄⢀⢀⢀⣠⣶⣄⢀⢀⣶⣶⣶⣶⣶⣄"+"\n"+
              "⣾⣿⠁⢀⢀⠉⠉⠉⢀⢀⣴⣿⡿⢿⣿⣦⡀⢀⣿⣿⡿⣷⣶⣾⢿⣿⣿⢀⢸⣿⡇⢀⢀⠉⠉"+"\n"+
              "⣿⣿⢀⢀⠠⣤⣤⣤⢀⢸⣿⣇⣀⣀⣸⣿⡇⢀⣿⣿⡇⠈⠿⠁⢸⣿⣿⢀⢸⣿⡿⠿⠇"+"\n"+
              "⠻⣿⣄⢀⢀⢹⣿⡿⢀⢸⣿⡟⠛⠛⢻⣿⡇⢀⣿⣿⡇⢀⢀⢀⢸⣿⣿⢀⢸⣿⡇"+"\n"+
              "⢀⠈⠛⠛⠛⠛⠋⢀⢀⠈⠛⠃⢀⢀⠘⠛⠁⢀⠉⠛⠃⢀⢀⢀⠘⠛⠉⢀⠈⠛⠛⠛⠛⠛⠋"+"\n"+
              "\n"+
              "⢀⢀⣠⣾⠿⠿⢿⣦⡀⢀⢀⣾⣿⢀⢀⣾⣷⡆⢀⣾⣿⠿⠿⢿⣷⡆⢀⣾⣿⠿⠿⢿⣦⡀"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡆⢀⣿⣿⢀⢀⣿⣿⡇⢀⣿⣿⣀⣀⢀⢀⢀⢀⣿⣿⢀⢀⣠⣿⠇"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡇⢀⢿⣿⢀⢀⣿⣿⠇⢀⣿⣿⠛⠛⢀⢀⢀⢀⣿⣿⣷⣾⡟⠁"+"\n"+
              "⢀⠈⠻⣷⣤⣤⣴⣿⠋⠁⢀⠘⢿⣦⣴⣿⠃⢀⢀⣿⣿⣤⣤⣴⣶⡆⢀⣿⣿⠉⢻⣿⣦⡀"+"\n"+
              "⢀⢀⢀⠈⠉⠉⠉⢀⢀⢀⢀⢀⢀⠉⠉⢀⢀⢀⢀⠈⠉⠉⠉⠉⠁⢀⢀⠈⠉⢀⢀⠈⠉⠁"+
              "\n");
  }

  @override
  int frostCape() {
     print("EL Drago Use Skill FROSTCAPE!!");
    var FC = 30;
    return FC;

  }

  @override
  int rage() {
    var R = 10;
    return R;
  }

}