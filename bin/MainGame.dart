import 'dart:io';
import 'dart:core';
import 'dart:math';

import 'Chicharron.dart';
import 'EL_Drago.dart';
import 'EL_Rival.dart';
import 'Frosty.dart';
import 'Chicken_Terminator.dart';

void main(List<String> args) {
  Chicharron chicharron = Chicharron();
  EL_Drago eL_Drago = EL_Drago();
  EL_Rival eL_Rival = EL_Rival();
  Frosty frosty = Frosty();
  Chicken_Terminator chicken_Terminator = Chicken_Terminator();

  Choose_Chicken_Fighter(
      chicharron, eL_Drago, eL_Rival, frosty, chicken_Terminator);
}

void Choose_Chicken_Fighter(Chicharron chicharron, EL_Drago eL_Drago,
    EL_Rival eL_Rival, Frosty frosty, Chicken_Terminator chicken_Terminator) {
  chooseChicken();
  var chooseCharacter = stdin.readLineSync()!;
  if (chooseCharacter == "1") {
    chicharronFight(chicharron, chicken_Terminator);
  } else if (chooseCharacter == "2") {
    eL_DragoFight(eL_Drago, chicken_Terminator);
  } else if (chooseCharacter == "3") {
    eL_RivalFight(eL_Rival, chicken_Terminator);
  } else if (chooseCharacter == "4") {
    frostyFight(frosty, chicken_Terminator);
  } else {
    print(" ");
  }
}

void frostyFight(Frosty frosty, Chicken_Terminator chicken_terminator) {
  while (frosty.hp > 0 || chicken_terminator.hp > 0) {
    print("กด 1 เพื่อโจมตี");
    var hit = stdin.readLineSync()!;
    if (hit == "1") {
      print("Frosty Hp ${frosty.hp}");

      var h = chicken_terminator.hp - frosty.atk;
      print("Chicken Terminator Hp $h");
      chicken_terminator.hp = h;
      if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
          //  print("Chicken Terminator Hp $h");
            break;
          }
      if (frosty.hp <= 65) {
        print("ใช้ Skill FROSTCAPE ตอนนี้หรือไม่? yes/no");
        var useSkill = stdin.readLineSync()!;
        if (useSkill == "yes") {
          var FC2 =
              chicken_terminator.hp - frosty.frostCape();
          chicken_terminator.hp = FC2;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            //print("Chicken Terminator Hp $h");
            break;
          }
        } else if (useSkill == "no") {
          var FC2 = chicken_terminator.hp - (frosty.atk + frosty.rage());
          chicken_terminator.hp = FC2;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            break;
          }
        }
        
      }else if (frosty.hp <= 0){
        break;
      }
      else if (chicken_terminator.hp <= 0) {
        chicken_terminator.die();
        //print(chicken_terminator.hp);
        break;
      }
    } else {
      print("โจมตีพลาด");
    }
    print("Turn: Chicken Terminator(ไก่หุ่นยนต์รุ่น CH_800 จ้องจะเล่นคุณ");
    const chars = "123";

    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }

    var n = RandomString(1);

    if (n == "1") {
      var h1 = frosty.hp - chicken_terminator.atk;
      //print("Chicken Terminator Hp $h1");
      frosty.hp = h1;
      if (frosty.hp <= 0) {
        frosty.die();
        break;
      }
      print("Chicken Terminator โจมตีคุณ ");
    } else if (n == "2") {
      var BS1 = frosty.hp -
          (chicken_terminator.overFlow() + chicken_terminator.rage());
      frosty.hp = BS1;
      if (frosty.hp <= 0) {
        frosty.die();
        break;
      }
    } else {
      print("Chicken Terminator โจมตีพลาด");
    }
  }
}

void eL_RivalFight(EL_Rival eL_Rival, Chicken_Terminator chicken_terminator) {
  while (eL_Rival.hp > 0 || chicken_terminator.hp > 0) {
    print("กด 1 เพื่อโจมตี");
    var hit = stdin.readLineSync()!;
    if (hit == "1") {
      print("EL RivalHp ${eL_Rival.hp}");

      var h = chicken_terminator.hp - eL_Rival.atk;
      print("Chicken Terminator Hp $h");
      chicken_terminator.hp = h;
       if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
           // print("Chicken Terminator Hp $h");
            break;
       }
      if (eL_Rival.hp <= 65) {
        print("ใช้ Skill FLAMESPLASH ตอนนี้หรือไม่? yes/no");
        var useSkill = stdin.readLineSync()!;
        if (useSkill == "yes") {
          var FS2 = chicken_terminator.hp -eL_Rival.flameSplash() ;
          chicken_terminator.hp = FS2;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            //print("Chicken Terminator Hp $h");
            break;
          }
        } else if (useSkill == "no") {
          var BS2 = chicken_terminator.hp - (eL_Rival.atk + eL_Rival.rage());
          chicken_terminator.hp = BS2;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            break;
          }
        }
      }else if (eL_Rival.hp <= 0){
        break;
      }
      if (chicken_terminator.hp <= 0) {
        chicken_terminator.die();
        //print(chicken_terminator.hp);
        break;
      }
    } else {
      print("โจมตีพลาด");
    }
    print("Turn: Chicken Terminator(ไก่หุ่นยนต์รุ่น CH_800 จ้องจะเล่นคุณ");
    const chars = "123";

    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }

    var n = RandomString(1);

    if (n == "1") {
      var h1 = eL_Rival.hp - chicken_terminator.atk;
      //print("Chicken Terminator Hp $h1");
      eL_Rival.hp = h1;
      if (eL_Rival.hp <= 0) {
        eL_Rival.die();
        break;
      }
      print("Chicken Terminator โจมตีคุณ ");
    } else if (n == "2") {
      var BS1 = eL_Rival.hp - (chicken_terminator.overFlow() + chicken_terminator.rage());
      eL_Rival.hp = BS1;
      if (eL_Rival.hp <= 0) {
        eL_Rival.die();
        break;
      }
    } else {
      print("Chicken Terminator โจมตีพลาด");
    }
  }
}

void eL_DragoFight(EL_Drago eL_Drago, Chicken_Terminator chicken_terminator) {
  while (eL_Drago.hp > 0 || chicken_terminator.hp > 0) {
    print("กด 1 เพื่อโจมตี");
    var hit = stdin.readLineSync()!;
    if (hit == "1") {
      print("EL Drago Hp ${eL_Drago.hp}");

      var h = chicken_terminator.hp - eL_Drago.atk;
      print("Chicken Terminator Hp $h");
      chicken_terminator.hp = h;
      if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
           // print("Chicken Terminator Hp $h");
            break;
          }
      if (eL_Drago.hp <= 65) {
        print("ใช้ Skill DRAGONBREATHE ตอนนี้หรือไม่? yes/no");
        var useSkill = stdin.readLineSync()!;
        if (useSkill == "yes") {
          var DB2 = chicken_terminator.hp - eL_Drago.dragonBreathe() ;
          chicken_terminator.hp = DB2;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            //print("Chicken Terminator Hp $h");
            break;
          }
        } else if (useSkill == "no") {
          var BS2 = chicken_terminator.hp - (eL_Drago.atk + eL_Drago.rage());
          chicken_terminator.hp = BS2;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            break;
          }
        }
      }else if (eL_Drago.hp <= 0){
        break;
      }
      else if (chicken_terminator.hp <= 0) {
        chicken_terminator.die();
        //print(chicken_terminator.hp);
        break;
      }
    } else {
      print("โจมตีพลาด");
    }
    print("Turn: Chicken Terminator(ไก่หุ่นยนต์รุ่น CH_800 จ้องจะเล่นคุณ");
    const chars = "123";

    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }

    var n = RandomString(1);

    if (n == "1") {
      var h1 = eL_Drago.hp - chicken_terminator.atk;
      //print("Chicken Terminator Hp $h1");
      eL_Drago.hp = h1;
      if (eL_Drago.hp <= 0) {
        eL_Drago.die();
        break;
      }
      print("Chicken Terminator โจมตีคุณ ");
    } else if (n == "2") {
      var BS1 = eL_Drago.hp -(chicken_terminator.overFlow() + chicken_terminator.rage());
      eL_Drago.hp = BS1;
      if (eL_Drago.hp <= 0) {
        eL_Drago.die();
        break;
      }
    } else {
      print("Chicken Terminator โจมตีพลาด");
    }
  }
}

void chicharronFight(
    Chicharron chicharron, Chicken_Terminator chicken_terminator) {
  while (chicharron.hp > 0 || chicken_terminator.hp > 0) {
    print("กด 1 เพื่อโจมตี");
    var hit = stdin.readLineSync()!;
    if (hit == "1") {
      print("Chicharron Hp ${chicharron.hp}");

      var h = chicken_terminator.hp - chicharron.atk;
      print("Chicken Terminator Hp $h");
      chicken_terminator.hp = h;
      if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
          //  print("Chicken Terminator Hp $h");
            break;
          }
      if (chicharron.hp <= 65) {
        print("ใช้ Skill BERSERK ตอนนี้หรือไม่? yes/no");
        var useSkill = stdin.readLineSync()!;
        if (useSkill == "yes") {
          var BS1 = chicken_terminator.hp - chicharron.berSerk() ;
          chicken_terminator.hp = BS1;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            //print("Chicken Terminator Hp $h");
            break;
          }
        } else if (useSkill == "no") {
          var BS2 = chicken_terminator.hp - (chicharron.atk + chicharron.rage());
          chicken_terminator.hp = BS2;
          if (chicken_terminator.hp <= 0) {
            chicken_terminator.die();
            break;
          }
        }
     }else if (chicharron.hp <= 0){
        break;
     }
      if (chicken_terminator.hp <= 0) {
        chicken_terminator.die();
       //print(chicken_terminator.hp);
        break;
      }
    } else {
      print("โจมตีพลาด");
    }
    print("Turn: Chicken Terminator(ไก่หุ่นยนต์รุ่น CH_800 จ้องจะเล่นคุณ)");
    const chars = "123";

    String RandomString(int strlen) {
      Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
      String result = "";
      for (var i = 0; i < strlen; i++) {
        result += chars[rnd.nextInt(chars.length)];
      }
      return result;
    }

    var n = RandomString(1);

    if (n == "1") {
      var h1 = chicharron.hp - chicken_terminator.atk;
      //print("Chicken Terminator Hp $h1");
      chicharron.hp = h1;
      if (chicharron.hp <= 0) {
        chicharron.die();
        break;
      }
    } else if (n == "2") {
      var BS1 = chicharron.hp - (chicken_terminator.overFlow() + chicken_terminator.rage());
      chicharron.hp = BS1;
      if (chicharron.hp <= 0) {
        chicharron.die();
        break;
      }
    } else {
      print("Chicken Terminator โจมตีพลาด");
    }
  }
}

void chooseChicken() {
  print(" " + "\n"
  +"    ⢀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀"+ "\n"
  +"⢀⡰⠎⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠱⢆⡀"+"\n"
  +"⣿⡇                Choose your fighter chicken                   ⢸⣿"+"\n"
  +"⣿⡇      1.Chicharron  HP: 200   ATK: 15   SKILL: BerSerk        ⢸⣿"+"\n"
  +"⣿⡇      2.EL Drago    HP: 220   ATK: 12   SKILL: DragonBreathe  ⢸⣿"+"\n"
  +"⣿⡇      3.EL Rival    HP: 230   ATK: 10   SKILL: FlameSplash    ⢸⣿"+"\n"
  +"⣿⡇      4.Frosty      HP: 220   ATK: 10   SKILL: FrostCape      ⢸⣿"+"\n"
  +"⠈⠱⢆⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⡰⠎⠁"+"\n"
  +"   ⠈⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉"+"\n"
  );
}
