import 'DragonBreathe.dart';
import 'Fighter.dart';

class EL_Drago implements DragonBreathe, Fighter {
  @override
  int atk = 12;

  @override
  int hp = 220;

  @override
  int skill = 0;

  @override
  int attack() {
    var atk = 12 ;
    return atk;
  }

  @override
  void die() {
   print("\n"+
              "⢀⢀⣶⣶⣶⣶⣆⢀⢀⢀⢀⢀⣴⣦⡀⢀⢀⢀⣠⣶⣄⢀⢀⢀⣠⣶⣄⢀⢀⣶⣶⣶⣶⣶⣄"+"\n"+
              "⣾⣿⠁⢀⢀⠉⠉⠉⢀⢀⣴⣿⡿⢿⣿⣦⡀⢀⣿⣿⡿⣷⣶⣾⢿⣿⣿⢀⢸⣿⡇⢀⢀⠉⠉"+"\n"+
              "⣿⣿⢀⢀⠠⣤⣤⣤⢀⢸⣿⣇⣀⣀⣸⣿⡇⢀⣿⣿⡇⠈⠿⠁⢸⣿⣿⢀⢸⣿⡿⠿⠇"+"\n"+
              "⠻⣿⣄⢀⢀⢹⣿⡿⢀⢸⣿⡟⠛⠛⢻⣿⡇⢀⣿⣿⡇⢀⢀⢀⢸⣿⣿⢀⢸⣿⡇"+"\n"+
              "⢀⠈⠛⠛⠛⠛⠋⢀⢀⠈⠛⠃⢀⢀⠘⠛⠁⢀⠉⠛⠃⢀⢀⢀⠘⠛⠉⢀⠈⠛⠛⠛⠛⠛⠋"+"\n"+
              "\n"+
              "⢀⢀⣠⣾⠿⠿⢿⣦⡀⢀⢀⣾⣿⢀⢀⣾⣷⡆⢀⣾⣿⠿⠿⢿⣷⡆⢀⣾⣿⠿⠿⢿⣦⡀"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡆⢀⣿⣿⢀⢀⣿⣿⡇⢀⣿⣿⣀⣀⢀⢀⢀⢀⣿⣿⢀⢀⣠⣿⠇"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡇⢀⢿⣿⢀⢀⣿⣿⠇⢀⣿⣿⠛⠛⢀⢀⢀⢀⣿⣿⣷⣾⡟⠁"+"\n"+
              "⢀⠈⠻⣷⣤⣤⣴⣿⠋⠁⢀⠘⢿⣦⣴⣿⠃⢀⢀⣿⣿⣤⣤⣴⣶⡆⢀⣿⣿⠉⢻⣿⣦⡀"+"\n"+
              "⢀⢀⢀⠈⠉⠉⠉⢀⢀⢀⢀⢀⢀⠉⠉⢀⢀⢀⢀⠈⠉⠉⠉⠉⠁⢀⢀⠈⠉⢀⢀⠈⠉⠁"+
              "\n");
  }

  @override
  int dragonBreathe() {
    print("EL Drago Use Skill DRAGONBREATHE!!");
    var DB = 35;
    return DB;

  }

  @override
  int rage() {
    var R = 10;
    return R;
  }
  
}
