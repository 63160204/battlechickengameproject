import 'FlameSplash.dart';
import 'Fighter.dart';

class EL_Rival implements FlameSplash, Fighter{
  @override
  int atk = 10;

  @override
  int hp = 230;

  @override
  int skill = 0;

  @override
  int attack() {
    var atk = 10;
    return atk;
  }

  @override
  void die() {
   print("\n"+
              "⢀⢀⣶⣶⣶⣶⣆⢀⢀⢀⢀⢀⣴⣦⡀⢀⢀⢀⣠⣶⣄⢀⢀⢀⣠⣶⣄⢀⢀⣶⣶⣶⣶⣶⣄"+"\n"+
              "⣾⣿⠁⢀⢀⠉⠉⠉⢀⢀⣴⣿⡿⢿⣿⣦⡀⢀⣿⣿⡿⣷⣶⣾⢿⣿⣿⢀⢸⣿⡇⢀⢀⠉⠉"+"\n"+
              "⣿⣿⢀⢀⠠⣤⣤⣤⢀⢸⣿⣇⣀⣀⣸⣿⡇⢀⣿⣿⡇⠈⠿⠁⢸⣿⣿⢀⢸⣿⡿⠿⠇"+"\n"+
              "⠻⣿⣄⢀⢀⢹⣿⡿⢀⢸⣿⡟⠛⠛⢻⣿⡇⢀⣿⣿⡇⢀⢀⢀⢸⣿⣿⢀⢸⣿⡇"+"\n"+
              "⢀⠈⠛⠛⠛⠛⠋⢀⢀⠈⠛⠃⢀⢀⠘⠛⠁⢀⠉⠛⠃⢀⢀⢀⠘⠛⠉⢀⠈⠛⠛⠛⠛⠛⠋"+"\n"+
              "\n"+
              "⢀⢀⣠⣾⠿⠿⢿⣦⡀⢀⢀⣾⣿⢀⢀⣾⣷⡆⢀⣾⣿⠿⠿⢿⣷⡆⢀⣾⣿⠿⠿⢿⣦⡀"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡆⢀⣿⣿⢀⢀⣿⣿⡇⢀⣿⣿⣀⣀⢀⢀⢀⢀⣿⣿⢀⢀⣠⣿⠇"+"\n"+
              "⢀⢸⣿⡇⢀⢀⢀⣿⣿⡇⢀⢿⣿⢀⢀⣿⣿⠇⢀⣿⣿⠛⠛⢀⢀⢀⢀⣿⣿⣷⣾⡟⠁"+"\n"+
              "⢀⠈⠻⣷⣤⣤⣴⣿⠋⠁⢀⠘⢿⣦⣴⣿⠃⢀⢀⣿⣿⣤⣤⣴⣶⡆⢀⣿⣿⠉⢻⣿⣦⡀"+"\n"+
              "⢀⢀⢀⠈⠉⠉⠉⢀⢀⢀⢀⢀⢀⠉⠉⢀⢀⢀⢀⠈⠉⠉⠉⠉⠁⢀⢀⠈⠉⢀⢀⠈⠉⠁"+
              "\n");
  }

  @override
  int flameSplash() {
    print("EL Rival Use Skill FLAMESPLASH!!");
    var FS = 25;
    return FS;

  }

  @override
  int rage() {
    var R = 10;
    return R;
  }

}